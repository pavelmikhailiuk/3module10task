package visitor.account;

import java.util.Map;

import visitor.domain.unit.Company;

public interface Accountant {
	Map<String, Double> visit(Company company);
}
