package visitor.account.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import visitor.account.Accountant;
import visitor.domain.unit.Company;
import visitor.domain.unit.Employee;

public class AverageSalaryAccountant implements Accountant {

	@Override
	public Map<String, Double> visit(Company company) {
		Map<String, Double> result = new HashMap<>();
		double salary = 0;
		List<Employee> employees = company.getEmployees();
		for (Employee employee : employees) {
			salary += employee.getSalary();
		}
		result.put("Average salary", salary / employees.size());
		return result;
	}
}
