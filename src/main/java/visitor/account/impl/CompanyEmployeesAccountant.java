package visitor.account.impl;

import java.util.HashMap;
import java.util.Map;

import visitor.account.Accountant;
import visitor.domain.unit.Company;

public class CompanyEmployeesAccountant implements Accountant {

	@Override
	public Map<String, Double> visit(Company company) {
		Map<String, Double> result = new HashMap<>();
		result.put("Total employees", Double.valueOf(company.getEmployees().size()));
		return result;
	}
}
