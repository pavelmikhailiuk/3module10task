package visitor.account.impl;

import java.util.HashMap;
import java.util.Map;

import visitor.account.Accountant;
import visitor.domain.unit.Company;
import visitor.domain.unit.Employee;

public class DepartmentEmployeesAccountant implements Accountant {

	@Override
	public Map<String, Double> visit(Company company) {
		Map<String, Double> result = new HashMap<>();
		for (Employee employee : company.getEmployees()) {
			String department = employee.getDepartment();
			Double deptEmployees = result.get(department);
			if (deptEmployees == null) {
				result.put(department, Double.valueOf(1));
			} else {
				result.put(department, deptEmployees + 1);
			}
		}
		return result;
	}
}
