package visitor.account.impl;

import java.util.HashMap;
import java.util.Map;

import visitor.account.Accountant;
import visitor.domain.unit.Company;
import visitor.domain.unit.Employee;

public class SalaryRaiseAccountant implements Accountant {

	private Map<String, Double> salaryDataBase;

	@Override
	public Map<String, Double> visit(Company company) {
		initDataBase();
		Map<String, Double> result = new HashMap<>();
		for (Employee employee : company.getEmployees()) {
			Double newsalary = employee.getSalary();
			Double oldSalary = salaryDataBase.get(employee.getName());
			result.put(employee.getName(), (newsalary - oldSalary) / oldSalary * 100);
		}
		return result;
	}

	private void initDataBase() {
		salaryDataBase = new HashMap<>();
		salaryDataBase.put("Jonhy Walker", 600.00);
		salaryDataBase.put("Jack Daniels", 800.00);
		salaryDataBase.put("William Grant", 500.00);
	}
}
