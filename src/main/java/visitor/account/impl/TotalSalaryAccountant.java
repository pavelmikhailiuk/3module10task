package visitor.account.impl;

import java.util.HashMap;
import java.util.Map;

import visitor.account.Accountant;
import visitor.domain.unit.Company;
import visitor.domain.unit.Employee;

public class TotalSalaryAccountant implements Accountant {

	@Override
	public Map<String, Double> visit(Company company) {
		Map<String, Double> result = new HashMap<>();
		double totalSalary = 0;
		for (Employee employee : company.getEmployees()) {
			totalSalary += employee.getSalary();
		}
		result.put("Total salary", totalSalary);
		return result;
	}
}
