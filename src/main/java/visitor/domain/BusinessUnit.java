package visitor.domain;

import java.util.Map;

import visitor.account.Accountant;

public interface BusinessUnit {
	Map<String, Double> accept(Accountant accountant);
}
