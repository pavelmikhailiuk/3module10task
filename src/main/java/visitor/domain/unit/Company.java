package visitor.domain.unit;

import java.util.List;
import java.util.Map;

import visitor.account.Accountant;
import visitor.domain.BusinessUnit;

public class Company implements BusinessUnit {

	private String name;

	private List<Employee> employees;

	public Company() {
	}

	public Company(String name, List<Employee> employees) {
		this.name = name;
		this.employees = employees;
	}

	@Override
	public Map<String, Double> accept(Accountant accountant) {
		return accountant.visit(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((employees == null) ? 0 : employees.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (employees == null) {
			if (other.employees != null)
				return false;
		} else if (!employees.equals(other.employees))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Company [name=" + name + ", employees=" + employees + "]";
	}
}
