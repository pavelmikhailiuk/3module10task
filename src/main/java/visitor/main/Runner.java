package visitor.main;

import java.util.Arrays;

import visitor.account.Accountant;
import visitor.account.impl.AverageSalaryAccountant;
import visitor.account.impl.CompanyEmployeesAccountant;
import visitor.account.impl.DepartmentEmployeesAccountant;
import visitor.account.impl.SalaryRaiseAccountant;
import visitor.account.impl.TotalSalaryAccountant;
import visitor.domain.unit.Company;
import visitor.domain.unit.Employee;

public class Runner {
	public static void main(String[] args) {
		Employee employee = new Employee("Jonhy Walker", 700.00, "TR-CPL");
		Employee employee1 = new Employee("Jack Daniels", 1000.00, "TR-CPL");
		Employee employee2 = new Employee("William Grant", 800.00, "TR");
		Company company = new Company("Epam", Arrays.asList(employee, employee1, employee2));
		Accountant accountant = new AverageSalaryAccountant();
		System.out.println(company.accept(accountant));
		accountant = new CompanyEmployeesAccountant();
		System.out.println(company.accept(accountant));
		accountant = new DepartmentEmployeesAccountant();
		System.out.println(company.accept(accountant));
		accountant = new SalaryRaiseAccountant();
		System.out.println(company.accept(accountant));
		accountant = new TotalSalaryAccountant();
		System.out.println(company.accept(accountant));
	}
}
